export default function initCreditCardMask() {
  const creditCardInputs = document.querySelectorAll(
    '[data-mask="credit-card"]'
  );

  creditCardInputs.forEach((creditCard) => {
    creditCard.addEventListener("input", handleInputChange);
    creditCard.addEventListener("keydown", handleKeyDown);
  });

  function handleInputChange(e) {
    let inputValue = e.target.value.replace(/\D/g, "");
    inputValue = inputValue.substring(0, 16);
    const formattedValue = formatCreditCard(inputValue);
    e.target.value = formattedValue;
  }

  function handleKeyDown(e) {
    const allowedKeys = [
      "Backspace",
      "Delete",
      "ArrowLeft",
      "ArrowRight",
      "Tab",
    ];
    if (!/^\d$/.test(e.key) && !allowedKeys.includes(e.key)) {
      e.preventDefault();
    }
  }

  function formatCreditCard(creditCard) {
    var creditCardRegex = /^(\d{4})(\d{4})(\d{4})(\d{4})$/;
    return creditCard.replace(creditCardRegex, "$1 $2 $3 $4");
  }
}
