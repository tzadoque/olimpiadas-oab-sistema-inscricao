export default function initDateMask() {
  const dateInputs = document.querySelectorAll('[data-mask="date"]');

  dateInputs.forEach((date) => {
    date.addEventListener("input", handleInputChange);
    date.addEventListener("keydown", handleKeyDown);
  });

  function handleInputChange(e) {
    let inputValue = e.target.value.replace(/\D/g, "");
    inputValue = inputValue.substring(0, 4);
    const formattedValue = formatMonthYear(inputValue);
    e.target.value = formattedValue;
  }

  function handleKeyDown(e) {
    const allowedKeys = [
      "Backspace",
      "Delete",
      "ArrowLeft",
      "ArrowRight",
      "Tab",
    ];
    if (!/^\d$/.test(e.key) && !allowedKeys.includes(e.key)) {
      e.preventDefault();
    }
  }

  function formatMonthYear(inputValue) {
    if (inputValue.length > 2) {
      inputValue = inputValue.substring(0, 2) + "/" + inputValue.substring(2);
    }
    return inputValue;
  }
}
