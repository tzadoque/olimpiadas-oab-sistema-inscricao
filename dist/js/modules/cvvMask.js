export default function initCvvMask() {
  const cvvInputs = document.querySelectorAll('[data-mask="cvv"]');

  cvvInputs.forEach((cvv) => {
    cvv.addEventListener("input", handleInputChange);
    cvv.addEventListener("keydown", handleKeyDown);
  });

  function handleInputChange(e) {
    // remove all non-digits
    let inputValue = e.target.value.replace(/\D/g, "");

    // limit input to 3 characters
    inputValue = inputValue.substring(0, 3);

    // format input with no separator
    const formattedValue = inputValue;
    e.target.value = formattedValue;
  }

  function handleKeyDown(e) {
    // allow only numbers, backspace, delete, and tab
    const allowedKeys = [
      "Backspace",
      "Delete",
      "ArrowLeft",
      "ArrowRight",
      "Tab",
    ];
    if (!/^\d$/.test(e.key) && !allowedKeys.includes(e.key)) {
      e.preventDefault();
    }
  }
}
