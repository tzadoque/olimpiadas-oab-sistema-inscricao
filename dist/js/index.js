import initAlert from "./modules/alert.js";
import initCreditCardMask from "./modules/creditCardMask.js";
import initDateMask from "./modules/dateMask.js";
import initCvvMask from "./modules/cvvMask.js";
import initDisableButton from "./modules/disableButton.js";
import initHidePassword from "./modules/hidePassword.js";
import initModal from "./modules/modal.js";
import initCopyPasteButton from "./modules/copyPasteButton.js";

initModal();
initHidePassword();
initDisableButton();
initAlert();
initCreditCardMask();
initDateMask();
initCvvMask();
initCopyPasteButton();
